BFG Repo-Cleaner
================

_Removes large or troublesome blobs like git-filter-branch does, but faster - and written in Scala_

```
$ bfg --strip-blobs-bigger-than 1M --replace-text banned.txt repo.git
```

The BFG is a simpler, faster (10 - 720x faster)
alternative to `git-filter-branch` for cleansing bad data out of your Git repository:

* Removing **Crazy Big Files**
* Removing **Passwords, Credentials** & other **Private data**

Main documentation for The BFG is here : **https://rtyley.github.io/bfg-repo-cleaner/**

Functionally-equivalent, apolitical fork of https://github.com/rtyley/bfg-repo-cleaner.git
